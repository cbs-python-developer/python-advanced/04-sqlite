"""Homework 4-5 | SQL scripts for exchange rates."""


create_table = """
    CREATE TABLE IF NOT EXISTS exchange_rate_to_usd (
        id integer PRIMARY KEY,
        currency_name text NOT NULL,
        currency_value real NOT NULL,
        last_updated text NOT NULL
    );
"""

insert_record = """
    INSERT INTO
        exchange_rate_to_usd (currency_name, currency_value, last_updated)
    VALUES
        (?, ?, ?);
"""
