"""Homework 4-5 | Exchange rates to USD.
Find the exchange rates using API Monobank."""

from datetime import datetime
from pathlib import Path
import requests
from typing import Optional

from hw414_personal_budget import db_handler as db
import sql_scripts as sql


DB_FILE = Path('./data/currency.db')
API_ENDPOINT = 'https://api.monobank.ua/bank/currency'
CURRENCY_CODES = {
    826: 'GBP',
    392: 'JPY',
    756: 'CHF',
    156: 'CNY',
}


class TooManyRequests(Exception):
    pass


def api_request(api_endpoint: str) -> Optional[dict]:
    """Request exchange rates from API."""
    response = requests.get(api_endpoint)
    if response.status_code == 200:
        return response.json()
    if response.status_code == 429:
        raise TooManyRequests('Too many requests to API. Please try later')


def get_usd_curr_rate(usd_uah_rate: float, curr_uah_rate: float) -> float:
    """Calculate average USD/CURRENCY rate based on UAH cross rates."""
    return usd_uah_rate / curr_uah_rate


def convert_date(date: int) -> str:
    """Convert date to ISO 8601 string ('YYYY-MM-DD HH:MM:SS')."""
    date_obj = datetime.fromtimestamp(date)
    return date_obj.strftime('%Y-%m-%d %H:%M:%S')


def main(db_file: Path, api_address: str) -> None:
    """Main function of the program."""

    if not db_file.exists():
        db.initialize_db(db_file, sql.create_table)

    rates = api_request(api_address)
    usd_uah_rate = rates[0]['rateSell']
    usd_rates = [
        ('UAH', usd_uah_rate, convert_date(rates[0]['date']))
    ]
    for rec in rates:
        if rec['currencyCodeA'] in CURRENCY_CODES:
            curr_name = CURRENCY_CODES[rec['currencyCodeA']]
            usd_curr_rate = get_usd_curr_rate(usd_uah_rate, rec['rateCross'])
            curr_date = convert_date(rec['date'])
            usd_rates.append(
                (curr_name, round(usd_curr_rate, 4), curr_date)
            )

    db_connection = db.create_connection(db_file)
    if db_connection is not None:
        db_connection.executemany(sql.insert_record, usd_rates)
        db_connection.commit()
        db_connection.close()


if __name__ == '__main__':
    main(DB_FILE, API_ENDPOINT)

    select_table = """SELECT * FROM exchange_rate_to_usd;"""
    with db.create_connection(DB_FILE) as connection:
        result = db.execute_select(connection, select_table)
    if result is not None:
        for row in result:
            print(row)
