"""Homework 4-1,2,3,4 | SQL scripts for personal budget."""


create_table = """
    CREATE TABLE IF NOT EXISTS budget (
        id integer PRIMARY KEY,
        item text NOT NULL,
        item_type text CHECK (item_type IN ('expense', 'income')),
        amount real NOT NULL,
        date text
    );
"""

insert_record = """
    INSERT INTO
        budget (item, item_type, amount, date)
    VALUES
        (:item, :item_type, :amount, :date)
"""

min_date = """SELECT MIN(date) AS min_date FROM budget"""

max_date = """SELECT MAX(date) AS max_date FROM budget"""

retrieve_data = """
    SELECT item_type, SUM(amount) AS amount
    FROM budget
    WHERE item_type IN ({item_types}) AND
        date BETWEEN :start_date AND :end_date
    GROUP BY item_type;
"""
