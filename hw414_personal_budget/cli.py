"""Homework 4-1,2,3,4 | CLI for personal budget."""

import argparse

from personal_budget import add_item, retrieve_info


parser = argparse.ArgumentParser(
    description='Simple personal budget.',
    formatter_class=argparse.RawTextHelpFormatter
)
subparsers = parser.add_subparsers(
    title='actions', help='operations with the budget'
)

# --ADD
add_parser = subparsers.add_parser('add', help='add a new record')
add_parser.set_defaults(func=add_item)
add_parser.add_argument(
    'item', type=str, metavar='ITEM', help='item name'
)
item_type_group = add_parser.add_argument_group('item type')
exclusive_group = item_type_group.add_mutually_exclusive_group(required=True)
exclusive_group.add_argument(
    '-e', '--expense', action='store_const', const='expense'
)
exclusive_group.add_argument(
    '-i', '--income', action='store_const', const='income'
)
add_parser.add_argument(
    '-a', '--amount', dest='amount', type=float, required=True,
    metavar='AMOUNT', help='an amount of money'
)
add_parser.add_argument(
    '-d', '--date', dest='date', type=str, metavar='DATE',
    help='date of transaction, format: YYYY-MM-DD, default: today'
)

# --VIEW
view_parser = subparsers.add_parser('view', help='view budget info')
view_parser.set_defaults(func=retrieve_info)
item_type_group = view_parser.add_argument_group(
    'item type to display',
    'if both are provided, the balance will be displayed as well'
)
item_type_group.add_argument(
    '-e', '--expense', action='store_const', const='expense'
)
item_type_group.add_argument(
    '-i', '--income', action='store_const', const='income'
)
view_parser.add_argument(
    '-d', '--date', dest='date', nargs=2, type=str,
    metavar=('START_DATE', 'END_DATE'),
    help='defines time period %(metavar)s to display, date format: YYYY-MM-DD, '
         'default: all available data'
)

arguments: argparse.Namespace = parser.parse_args()
