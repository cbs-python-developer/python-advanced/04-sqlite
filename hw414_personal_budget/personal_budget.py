"""Homework 4-1,2,3,4 | Personal budget using SQLite."""

import argparse
from datetime import date
from pathlib import Path
import sqlite3

import cli
import db_handler as db
import budget_sql_scripts as sql


DB_FILE = Path('./data/budget.db')


def add_item(
        args: argparse.Namespace, connection: sqlite3.Connection
) -> None:
    """Add an item to the budget table."""

    insert_data = {
        'item': args.item,
        'item_type': args.income or args.expense,
        'amount': args.amount
    }
    # --date
    if args.date is None:
        insert_data['date'] = date.today().strftime('%Y-%m-%d')
    else:
        insert_data['date'] = args.date

    db.execute_query(connection, sql.insert_record, insert_data)


def prepare_output(amounts: dict[str, float]) -> str:
    """Prepare the output to display to the user."""
    output = '\n'.join(
        f"{item_type.title():>8}: {amount:>8.2f}"
        for item_type, amount in amounts.items()
    )
    if len(amounts) == 2:
        balance = amounts['income'] - amounts['expense']
        output += f"\n Balance: {balance:>8.2f}"
    return output


def retrieve_info(
        args: argparse.Namespace, connection: sqlite3.Connection
) -> None:
    """Retrieve the budget info."""

    # --date
    if args.date is None:
        start_date = db.execute_select(connection, sql.min_date)[0]['min_date']
        end_date = db.execute_select(connection, sql.max_date)[0]['max_date']
    else:
        start_date = args.date[0]
        end_date = args.date[1]
    select_data = {
        'start_date': start_date,
        'end_date': end_date
    }
    # --item type
    item_types = [
        f"'{item_type}'"
        for item_type in (args.expense, args.income)
        if item_type is not None
    ]
    item_types = ', '.join(item_types)

    sql_statement = sql.retrieve_data.format(item_types=item_types)
    result = db.execute_select(connection, sql_statement, select_data)
    if result is not None:
        amounts = {
            row['item_type']: row['amount'] for row in result
        }
        print(prepare_output(amounts))


def main(args: argparse.Namespace, db_file: Path) -> None:
    """Main function of the program."""
    if not db_file.exists():
        db.initialize_db(db_file, sql.create_table)
    db_connection = db.create_connection(db_file)
    if db_connection is not None:
        args.func(args, db_connection)
        db_connection.close()


if __name__ == '__main__':
    main(cli.arguments, DB_FILE)
