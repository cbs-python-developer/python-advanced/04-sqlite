"""Set of functions to interact with the database."""

from pathlib import Path
import sqlite3
from sqlite3 import Error
from typing import Optional


def create_connection(db_file: Path) -> Optional[sqlite3.Connection]:
    """Create a database connection to the SQLite DB specified by db_file."""
    connection = None
    try:
        connection = sqlite3.connect(db_file)
        print("Connected to SQLite DB successfully")
    except Error as e:
        print(f"The error '{e}' occurred")
    return connection


def execute_query(
        connection: sqlite3.Connection,
        query: str,
        data: Optional[dict] = None
) -> None:
    """Execute the SQL query."""
    cursor = connection.cursor()
    try:
        if data is None:
            cursor.execute(query)
        else:
            cursor.execute(query, data)
        connection.commit()
        print("Query executed successfully")
    except Error as e:
        print(f"The error '{e}' occurred")


def execute_select(
        connection: sqlite3.Connection,
        query: str,
        data: Optional[dict] = None
) -> Optional[list[dict]]:
    """Execute the SQL SELECT query."""
    connection.row_factory = sqlite3.Row
    try:
        if data is None:
            query_result = connection.execute(query)
        else:
            query_result = connection.execute(query, data)
        field_names = [desc[0] for desc in query_result.description]
        return [
            {field: row[field] for field in field_names}
            for row in query_result
        ]
    except Error as e:
        print(f"The error '{e}' occurred")
        return None


def initialize_db(db_file: Path, query: str) -> None:
    """Initialize the DB table."""
    parent_dir = db_file.parent
    parent_dir.mkdir(parents=True, exist_ok=True)
    connection = create_connection(db_file)
    if connection is not None:
        execute_query(connection, query)
